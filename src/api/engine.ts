import { http } from "@/utils/http";

type Result = {
  segment: string;
  expr: string;
  type: string;
  child?: Array<any> /** 列表数据 */;
};

type TransferResult = {};

const buildPath = (path: string) => {
  const url = "http://localhost:8999";
  return url + path;
};

/** 转换数据 */
export const getMapperPaths = (data?: object) => {
  return http.request<Result>("post", buildPath("/mapper/paths"), { data });
};

export const transfer = (data?: object) => {
  return http.request<TransferResult>("post", buildPath("/mapper/transfer"), {
    data
  });
};
