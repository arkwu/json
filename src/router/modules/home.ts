const { VITE_HIDE_HOME } = import.meta.env;
const Layout = () => import("@/layout/index.vue");

export default {
  path: "/",
  name: "Home",
  component: Layout,
  redirect: "/welcome",
  meta: {
    icon: "homeFilled",
    title: "首页",
    rank: 0
  },
  children: [
    {
      path: "/welcome",
      name: "Welcome",
      component: () => import("@/views/welcome/index.vue"),
      meta: {
        title: "首页",
        showLink: VITE_HIDE_HOME === "true" ? false : true
      }
    },
    {
      path: "/test",
      name: "test",
      component: () => import("@/views/test/index.vue"),
      meta: {
        title: "测试"
      }
    },
    {
      path: "/test2",
      name: "test2",
      component: () => import("@/views/test/two.vue"),
      meta: {
        title: "测试2"
      }
    },
    {
      path: "/test3",
      name: "test3",
      component: () => import("@/views/test/three.vue"),
      meta: {
        title: "测试3"
      }
    },
    {
      path: "/test4",
      name: "test4",
      component: () => import("@/views/test/four.vue"),
      meta: {
        title: "测试4"
      }
    },
    {
      path: "/test5",
      name: "test5",
      component: () => import("@/views/test/five.vue"),
      meta: {
        title: "测试5"
      }
    },
    {
      path: "/test6",
      name: "test6",
      component: () => import("@/views/test/six.vue"),
      meta: {
        title: "测试6"
      }
    },
    {
      path: "/test7",
      name: "test7",
      component: () => import("@/views/test/seven.vue"),
      meta: {
        title: "测试7"
      }
    }
  ]
} as RouteConfigsTable;
