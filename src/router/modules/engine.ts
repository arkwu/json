const Layout = () => import("@/layout/index.vue");

export default {
  path: "/Engine",
  name: "Engine",
  component: Layout,
  redirect: "/engine",
  meta: {
    icon: "ph:engine",
    title: "引擎",
    rank: 2
  },
  children: [
    {
      path: "/engine",
      name: "Engine",
      component: () => import("@/views/engine/one.vue"),
      meta: {
        title: "引擎测试1"
      }
    },
    {
      path: "/engine2",
      name: "Engine2",
      component: () => import("@/views/engine/two.vue"),
      meta: {
        title: "引擎测试2"
      }
    },
    {
      path: "/engine3",
      name: "Engine3",
      component: () => import("@/views/engine/three.vue"),
      meta: {
        title: "引擎测试3"
      }
    },
    {
      path: "/engine4",
      name: "Engine4",
      component: () => import("@/views/engine/four.vue"),
      meta: {
        title: "引擎测试4"
      }
    },
    {
      path: "/engine5",
      name: "Engine5",
      component: () => import("@/views/engine/five.vue"),
      meta: {
        title: "引擎测试5"
      }
    },
    {
      path: "/engine6",
      name: "Engine6",
      component: () => import("@/views/engine/six.vue"),
      meta: {
        title: "引擎测试6"
      }
    },
    {
      path: "/engine7",
      name: "Engine7",
      component: () => import("@/views/engine/seven.vue"),
      meta: {
        title: "引擎测试7"
      }
    },
    {
      path: "/engine8",
      name: "Engine8",
      component: () => import("@/views/engine/eight.vue"),
      meta: {
        title: "引擎测试8"
      }
    },
    {
      path: "/engine9",
      name: "Engine9",
      component: () => import("@/views/engine/nine.vue"),
      meta: {
        title: "引擎测试9"
      }
    }
  ]
} as RouteConfigsTable;
